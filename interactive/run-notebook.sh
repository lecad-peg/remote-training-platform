#!/bin/bash
eval "$(conda shell.bash hook)"
conda activate cling
export LD_LIBRARY_PATH=${HOME}/miniconda3/envs/cling/lib
export LIBRARY_PATH=${HOME}/miniconda3/envs/cling/lib
jupyter notebook

## Notes
# sudo firewall-cmd --permanent --add-port=443/tcp --zone=public
# sed -i -e '/display_name/s/",/ OpenMP",/' -e '/-std=c++/s/$/, "-fopenmp"/' miniconda3/envs/*/share/jupyter/kernels/xcpp*/kernel.json
