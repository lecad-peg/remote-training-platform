# Remote Training Platform Image #

The objective of this repository is to provide a HPC Singularity image
for running desktop on HPC compute node interactively.

Ultimately, a cluster with many login nodes and remote desktop
capability should be available.

### What is this repository for? ###

* Quick summary
* Version


For a typical class of 40-80 participants I propose virtual training
cluster consisting of:

1. 20 login nodes with Infiniband and GPU. All login nodes should be 
   accessible directly on the internet and will be used as a computing partition. 
2. NoMachine workstation licences for all nodes providing persistent remote desktops 
   that can be shadowed to the educator. The cost is €1 500/year in total 
   (2 packs of 10 perpetual license https://www.nomachine.com/buyonline), 
   limiting max 4 participants per login node. 
   The login should be GPU supported, providing VirtualGL desktop natively for
   visualisation as well as GPU programming with all toolchains for training 
   provided by HLST support.
3. TurboVNC desktop servers for following slides 
   and desktop of the lecturer.
4. Hadoop filesystem on local login nodes consisting of a training
   portal for “big data” courses.
5. Jupyter HPC Hub, RStudio, and similar notebook services
6. Zoom license available to educators. Possibility to use 
   breakout rooms for working in pairs. Optional Slack for group chat.

In the following figure main components of the “training cluster” are
shown. Externally, Zoom video conferencing (or similar) is used for
general presentations from trainer to trainees (left side of the
figure). Zoom allows up to 50 breakout rooms for setting up pairs or
smaller groups of trainees to work together with hands-on
exercises. Groups can be supported by additional
instructors. Registration is handled as usual with PRACE Indico
(https://events.prace-ri.eu). Indico should allow registration of
participants by creating an account that can be reused at different
PRACE courses. Indico holds all necessary registration data for
authorization (admittance) to the course and provides credentials to
specific login node. Authentication can be further supported by
eduGAIN login that lowers the necessary identification of trainees.

![Remote Training Platform](platform.png)

Compute nodes are at the same time login nodes. Therefore, the
training cluster is a shared facility. Each node (only one fully shown
on the figure) consist of NoMachine (NX) remote desktop, GPU for
computing and VirtualGL acceleration, optional TurboVNC client-server
for additional intra-desktop collaboration or looking into lecturers
desktop or slides. Each compute and login node can behave as a data
node with storage for “big-data” classes (HDFS, Hadoop, Spark,
…). Apparently, such a “mini cluster” with many login nodes can be a
separate SLURM partition allowing to run short jobs across all 20
login nodes. Infiniband for MPI jobs is not ultimately
necessary. Internally to the cluster additional web services can be
provided to users such as Jupyter HPC hub or RStudio that can be
exposed also externally to MOOC classes if resource allocation is
provided programmatically.  Users on login nodes can run Web browsers
under the remote NX desktop and therefore all notebook-like creation
is easily allocated (e.g. through Anaconda navigator).

There can be more than one such training platform established and can
be further enhanced with request from educators getting the platform
allocated for specific courses and administered by HLST that may also
need to enhance AAA mechanism required for seamless mapping of
participants registered to courses or through FutureLearn
“Exercise” step.  Requested dedicated hardware resources can be
extracted from existing large clusters by reconfiguration of network,
nodes system software and installing requested servers for
desktop. Investment in commercial licences for NoMachine and Zoom are
minimal. 1PM of HLST administration effort for setting up and 1PM for
development of some web interfaces is estimated.  Proposal can be
easily scaled with larger numbers following demand.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin leon.kos@lecad.fs.uni-lj.si
* Other community or team contact
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)