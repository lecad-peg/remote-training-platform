# https://jupyter-notebook.readthedocs.io/en/stable/public_server.html#embedding-the-notebook-in-another-website
# https://github.com/jupyterhub/jupyterhub/issues/379#issuecomment-721015914
# https://github.com/jupyterhub/jupyterhub/issues/3162#issuecomment-689668002 for Chrome cookie options

c.NotebookApp.tornado_settings = {
    'headers': {
        'Content-Security-Policy': "frame-ancestors https://www.futurelearn.com 'self' https://static.cloudflareinsights.com; report-uri /api/security/csp-report ",
    }
}
c.NotebookApp.open_browser = False
c.NotebookApp.disable_check_xsrf = True
c.NotebookApp.webbrowser_open_new = 0
c.NotebookApp.allow_remote_access = True
c.NotebookApp.cookie_options = {'SameSite': 'None', 'Secure': True}
c.MappingKernelManager.cull_connected = False
c.MappingKernelManager.cull_idle_timeout = 600
c.MappingKernelManager.cull_interval = 200
c.AsyncMappingKernelManager.cull_idle_timeout = 600
c.AsyncMappingKernelManager.cull_interval = 200
c.NotebookApp.shutdown_no_activity_timeout = 3600
