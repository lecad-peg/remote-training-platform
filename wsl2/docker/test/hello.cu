#include <stdio.h>
 
__global__ void hello()
{
    printf("Hello World!\n");
}

int main(int argc,char **argv)
{
    // launch the kernel
    hello<<<1, 4>>>();
 
    // force the printf()s to flush
    cudaDeviceSynchronize();
 
    return 0;
}
