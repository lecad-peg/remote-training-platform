# Ansible for WSL2 

Controlling WSL2 through Ansible is only possible if the Windows port of OpenSSH 
allows sftp and scp subsystems. If having `bash` as default shell then subsystem
scp does not work. 

Therefore, Ansible on WSL2 nodes can't be used at this point.
