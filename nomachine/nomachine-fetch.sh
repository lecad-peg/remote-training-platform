#!/bin/sh
NOMACHINE_VERSION=6.10
NOMACHINE_PACKAGE_NAME=nomachine_6.10.12_1_amd64.deb
NOMACHINE_MD5=930ed68876b69a5a20f3f2b2c0650abc
curl -fSL "http://download.nomachine.com/download/${NOMACHINE_VERSION}/Linux/${NOMACHINE_PACKAGE_NAME}" -o nomachine.deb \
&& echo "${NOMACHINE_MD5} *nomachine.deb" | md5sum -c
