# Ineractive hands-on Introduction to Parallel Programming

Setting up xeus-cling server for interactive parallel exercises

Details of configuration:
- Windows 10 release 21H2
- NVIDIA Quadro P400 (Pascal) 2GB graphics
- Intel I5-8500 processor with integrated graphics enabled in BIOS for Intel OpenCL 3.0 with max 256 threads 
- WSL2 with Ubuntu 20.04
- Windows OpenSSH daemon with Bash shell
- CUDA toolkit 11.4 from NVIDIA

:warning: OpenCL does not work with NVIDIA WSL2 drivers! Intel Beignet OpenCL does not work either. Only Intel OpenCL is used instead.


## CUDA 11.4 install
See https://docs.nvidia.com/cuda/wsl-user-guide/index.html#ch03a-setting-up-cuda

~~~ bash
wget https://developer.download.nvidia.com/compute/cuda/repos/wsl-ubuntu/x86_64/cuda-wsl-ubuntu.pin
sudo mv cuda-wsl-ubuntu.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget https://developer.download.nvidia.com/compute/cuda/11.4.0/local_installers/cuda-repo-wsl-ubuntu-11-4-local_11.4.0-1_amd64.deb
sudo dpkg -i cuda-repo-wsl-ubuntu-11-4-local_11.4.0-1_amd64.deb
sudo apt-key add /var/cuda-repo-wsl-ubuntu-11-4-local/7fa2af80.pub
sudo apt-get update
sudo apt-get -y install cuda
~~~

## Docker install
Docker install instructions from [NVIDIA WSL2 docker page](https://docs.nvidia.com/cuda/wsl-user-guide/index.html#ch04-sub01-install-docker).
~~~bash
curl https://get.docker.com | sh
sudo apt-get install -y uidmap

dockerd-rootless-setuptool.sh install --skip-iptables
[INFO] systemd not detected, dockerd-rootless.sh needs to be started manually:

PATH=/usr/bin:/sbin:/usr/sbin:$PATH dockerd-rootless.sh  --iptables=false

[INFO] Creating CLI context "rootless"
Successfully created context "rootless"

[INFO] Make sure the following environment variables are set (or add them to ~/.bashrc):

# WARNING: systemd not found. You have to remove XDG_RUNTIME_DIR manually on everylogout.
export XDG_RUNTIME_DIR=/home/lecad/.docker/run
export PATH=/usr/bin:$PATH
export DOCKER_HOST=unix:///home/lecad/.docker/run/docker.sock

sudo service docker start
[sudo] password for lecad: 
 * Starting Docker: docker                                                  [ OK ]
 sudo usermod -a -G docker $USER
 #logout and relogin to get into docker group
~~~
Running CUDA inside container requires a tailored [Docker CUDA distribution](https://docs.nvidia.com/cuda/wsl-user-guide/index.html#ch05-running-containers).

~~~ bash
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
sudo apt-get install -y nvidia-docker2
sudo service docker stop
sudo service docker start
docker run --gpus all nvcr.io/nvidia/k8s/cuda-sample:nbody nbody -gpu -benchmark
~~~

### Docker shell 

    docker ps
    docker exec -u 0 0769192a1745 bash

## INTEL OpenCL install

https://github.com/intel/compute-runtime/releases/tag/21.35.20826

~~~ bash
mkdir neo
cd neo
wget https://github.com/intel/compute-runtime/releases/download/21.35.20826/intel-gmmlib_21.2.1_amd64.deb
wget https://github.com/intel/intel-graphics-compiler/releases/download/igc-1.0.8517/intel-igc-core_1.0.8517_amd64.deb
wget https://github.com/intel/intel-graphics-compiler/releases/download/igc-1.0.8517/intel-igc-opencl_1.0.8517_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/21.35.20826/intel-opencl_21.35.20826_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/21.35.20826/intel-ocloc_21.35.20826_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/21.35.20826/intel-level-zero-gpu_1.2.20826_amd64.deb
wget https://github.com/intel/compute-runtime/releases/download/21.35.20826/ww35.sum
sha256sum -c ww35.sum
sudo dpkg -i *.deb


docker run -it --device /dev/dxg:/dev/dxg --rm docker.io/intelopencl/intel-opencl:ubuntu-20.04-ppa clinfo

~~~

## Port forward

Windows [OpenSSH](https://github.com/powershell/win32-openssh/wiki) does not support port tunneling and therefore Windows firewall needs to be changed.
OpenSSH shellcan be set system-wide with `HKLM\Software\OpenSSH\DefaultShell`. If this key is deleted then detault PowerShell is used.
~~~ bash
sudo apt-get install -y python-is-python3 python3.8-venv
python -m http.server
~~~

With [WSLHostPatcher](https://github.com/CzBiX/WSLHostPatcher) we can directly expose any WSL2 port. `HostPatcher.exe` should be started inside WSL2 within Bash `.profile`script. HostPatcher can be started as a normal login user. Obtaining 
~~~bash
wget https://github.com/CzBiX/WSLHostPatcher/releases/download/v0.1.1/WSLHostPatcher.zip
sudo apt-get install unzip
unzip WSLHostPatcher.zip
./WSLHostPatcher.exe
python -m http.server
~~~


There needs to be open Inbound TCP port 8000 in Windows firewall by the administrator to allow the connection from outside. This firewall setting is permanent accros reboots. The easiest is to login as \pc-adminstrator or Run CMD As Administrator and then enter
~~cmd
    netsh advfirewall firewall add rule name="Open Port 8000" dir=in action=allow protocol=TCP localport=8000
~~

Other solutions are [discussed on Stackoverflow](
https://stackoverflow.com/questions/61002681/connecting-to-wsl2-server-via-local-network).

### Jupyter notebook test

~~~ bash
sudo apt-get install python3.8-venv
python3 -m venv notebook
notebook/bin/pip install jupyter-notebook
notebook/bin/jupyter-notebook --port 8000 --no-browser --NotebookApp.allow_remote_ac
cess=true
~~~

## Building Docker image

   make build

or

   docker build --rm --force-rm -t lecad/xeus-cling-notebook:latest .

## Running docker image

   make dev

See https://849.ablak.arnes.si/ or run locally as http://localhost/ with


   docker run -p 80:8888 lecad/xeus-cling-notebook

or

   docker run -p 80:8888 -v ${HOME}/ihipp-examples:/home/jovyan/work lecad/xeus-cling-notebook

to mount ${HOME}/ihipp-examples into docker image for writing examples.

## attaching to docker image as root

    docker ps
    docker exec -u 0 -it <id_of_running_image> bash



## LetsEncrypt 

From https://certbot.eff.org/docs/install.html#running-with-docker

	docker pull certbot/certbot
	sudo firewall-cmd --permanent --add-port=443/tcp --zone=public
	sudo firewall-cmd --permanent --add-port=80/tcp --zone=public

	sudo docker run -p 80:80 -it --rm --name certbot \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
            certbot/certbot certonly

Enter 1) temporary web and 849.ablak.arnes.si for doman name.

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/849.ablak.arnes.si/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/849.ablak.arnes.si/privkey.pem
   Your certificate will expire on 2021-07-13. To obtain a new or
   tweaked version of this certificate in the future, simply run
   certbot again. To non-interactively renew *all* of your
   certificates, run "certbot renew"

See https://jupyter-docker-stacks.readthedocs.io/en/latest/using/common.html
for starting with ceritficates instead of self signed certs locally

    docker run -e GEN_CERT=yes -p 443:8888 lecad/xeus-cling-notebook

    docker run -p 443:8888 \
    	   -v /etc/letsencrypt/archive/849.ablak.arnes.si:/etc/ssl/notebook \
    	   lecad/xeus-cling-notebook start-notebook.sh \
    	   --NotebookApp.certfile=/etc/ssl/notebook/fullchain1.pem \
	   --NotebookApp.keyfile=/etc/ssl/notebook/privkey1.pem

## Manual setup of the xeus-cling notebook with OpenMP and MPI

   wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
   chmod +x Miniconda3-latest-Linux-x86_64.sh
   ./Miniconda3-latest-Linux-x86_64.sh
   conda create -n cling
   conda activate cling
   conda install -c conda-forge notebook
   conda install -c conda-forge xeus-cling=0.12.1
   conda install openmpi -c conda-forge
   conda install openmp -c conda-forge
   sed -i -e '/display_name/s/",/ OpenMP and MPI",/' \
       -e '/-std=c++/s/$/, "-fopenmp"/' \
       ~/miniconda3/envs/*/share/jupyter/kernels/xcpp*/kernel.json
   git clone https://github.com/kosl/ihipp-examples.git
   ./run-notebook.sh
