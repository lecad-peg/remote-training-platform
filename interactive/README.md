# Ineractive hands-on Introduction to Parallel Programming

Setting up xeus-cling server for interactive parallel exercises

## Building Docker image

   make build

or

   docker build --rm --force-rm -t lecad/xeus-cling-notebook:latest .

## Running docker image

   make dev

See https://849.ablak.arnes.si/ or run locally as http://localhost/ with


   docker run -p 80:8888 lecad/xeus-cling-notebook

or

   docker run -p 80:8888 -v ${HOME}/ihipp-examples:/home/jovyan/work lecad/xeus-cling-notebook

to mount ${HOME}/ihipp-examples into docker image for writing examples.


## LetsEncrypt 

From https://certbot.eff.org/docs/install.html#running-with-docker

	docker pull certbot/certbot
	sudo firewall-cmd --permanent --add-port=443/tcp --zone=public
	sudo firewall-cmd --permanent --add-port=80/tcp --zone=public

	sudo docker run -p 80:80 -it --rm --name certbot \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
            certbot/certbot certonly

Enter 1) temporary web and 849.ablak.arnes.si for doman name.

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/849.ablak.arnes.si/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/849.ablak.arnes.si/privkey.pem
   Your certificate will expire on 2021-07-13. To obtain a new or
   tweaked version of this certificate in the future, simply run
   certbot again. To non-interactively renew *all* of your
   certificates, run "certbot renew"

See https://jupyter-docker-stacks.readthedocs.io/en/latest/using/common.html
for starting with ceritficates instead of self signed certs locally

    docker run -e GEN_CERT=yes -p 443:8888 lecad/xeus-cling-notebook

    docker run -p 443:8888 \
    	   -v /etc/letsencrypt/archive/849.ablak.arnes.si:/etc/ssl/notebook \
    	   lecad/xeus-cling-notebook start-notebook.sh \
    	   --NotebookApp.certfile=/etc/ssl/notebook/fullchain1.pem \
	   --NotebookApp.keyfile=/etc/ssl/notebook/privkey1.pem

## Manual setup of the xeus-cling notebook with OpenMP and MPI

   wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
   chmod +x Miniconda3-latest-Linux-x86_64.sh
   ./Miniconda3-latest-Linux-x86_64.sh
   conda create -n cling
   conda activate cling
   conda install -c conda-forge notebook
   conda install -c conda-forge xeus-cling=0.12.1
   conda install openmpi -c conda-forge
   conda install openmp -c conda-forge
   sed -i -e '/display_name/s/",/ OpenMP and MPI",/' \
       -e '/-std=c++/s/$/, "-fopenmp"/' \
       ~/miniconda3/envs/*/share/jupyter/kernels/xcpp*/kernel.json
   git clone https://github.com/kosl/ihipp-examples.git
   ./run-notebook.sh
