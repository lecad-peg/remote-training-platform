# https://jupyter-notebook.readthedocs.io/en/stable/public_server.html#embedding-the-notebook-in-another-website
# https://github.com/jupyterhub/jupyterhub/issues/379#issuecomment-721015914

c.NotebookApp.tornado_settings = {
    'headers': {
        'Content-Security-Policy': "frame-ancestors https://www.futurelearn.com 'self' "
    }
}
#c.NotebookApp.tokenUnicode = '7aa0bf315e16ea755b022570663581876e1b14f64f90f0fa'
