#!local/bin/python

from optparse import OptionParser
from paramiko.common import DEFAULT_MAX_PACKET_SIZE
from wakeonlan import send_magic_packet
import paramiko
import os
from urllib.request import urlopen
import re
import time
import datetime
import math
from multiprocessing import Process, Queue

interface='192.168.5.11'
image="kosl/ihipp:1.0.10"


hosts = { 
    60: "6C:0B:84:42:D1:D0",
    61: "4c:52:62:28:3b:0c",
    62: "4c:52:62:28:3a:e7",
    63: "4c:52:62:28:3a:b2",
    64: "4c:52:62:28:3a:b1",
    65: "4c:52:62:28:3b:88",
    66: "4c:52:62:28:3a:af",
    67: "4c:52:62:28:3a:be",
    68: "4c:52:62:28:3b:34",
    69: "4c:52:62:28:3a:e9",
    70: "4c:52:62:28:3a:b0",
    71: "4c:52:62:28:3a:ea",
    72: "4c:52:62:28:3a:ec",
    73: "4c:52:62:28:3a:eb",
    74: "4c:52:62:28:3a:bf",
    75: "4c:52:62:28:3a:e8",
    76: "4c:52:62:28:3b:2e",
    77: "4c:52:62:28:3a:b3",
    78: "4c:52:62:28:3a:b6",
}

all_hosts = [hosts[i] for i in range(61, 79)]

node_status = dict.fromkeys(range(61,79)) #node status: Offline, Online, Failed, Wait

def printformat(lines):
    for line in lines:
        print(line.format(), end = '')

def timestamp():
    return str(datetime.datetime.now())[:-4]

def ping(hpc: int) -> int:
    host = "hpc"+str(hpc)
    response = os.system(">/dev/null ping -q -w 2 -c 1 " + host)
    if response:
        print(host + " timed out")
        return False
    else:
        print(host + " pinged")
        return True


def ssh(host: str, command: str):
    #command += "; exit"
    print("Running command:" + command)
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(host, 22, "pc-user", "jijInCinea")
    stdin, stdout, stderr = client.exec_command(command, get_pty=False)
    lines = stdout.readlines()
    errors = stderr.readlines()
    printformat(lines)
    printformat(errors)
    client.close()

def stats(hpc: int):
    link = "http://hpc" + str(hpc) + ":8000/metrics?token=7aa0bf315e16ea755b022570663581876e1b14f64f90f0fa"
    try:
        f = urlopen(link, timeout=2)
    except:
        return None
    text = f.read()
    kernel_currently_running = re.findall(r"kernel_currently_running_total{\S*} \d+.\d+", str(text))
    if kernel_currently_running == None:
        return 0
    result = 0
    for match in kernel_currently_running:
        total = re.search(r"\d+.\d+", match)
        result += int(total.group(0)[:-2])
    return result

def run(hpc: int, statusQ): #Check and run docker image on one host
    host = "hpc"+str(hpc)
    #Login ssh
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(host, 22, "pc-user", "jijInCinea")
    #Check if WSL host patched
    stdin, stdout, stderr = client.exec_command("wsl ls /dev/shm/.hostpatched")
    lines = stdout.readlines()
    errors = stderr.readlines()
    if errors and 'No such file or directory' in errors[0]:
        print("hpc"+str(hpc)+"-Patching WSL.")
        stdin, stdout, stderr = client.exec_command("wsl '${HOME}/WSLHostPatcher.exe'")
        lines = stdout.readlines()
        errors = stderr.readlines()
        printformat(lines)
        printformat(errors)
        stdin, stdout, stderr = client.exec_command("wsl touch /dev/shm/.hostpatched")
        lines = stdout.readlines()
        errors = stderr.readlines()
        printformat(lines)
        printformat(errors)
    #Check if docker running
    stdin, stdout, stderr = client.exec_command("wsl service docker status")
    #stdin.writelines(["pwd"])
    #stdin.close()
    lines = stdout.readlines()
    errors = stderr.readlines()
    if errors:
        printformat(errors)
        client.close()
        return
    #What to do. If True, then need to start Docker service. 
    start = True
    if 'Windows Subsystem for Linux has no installed distributions.' in lines[0]:
        print("hpc"+str(hpc)+"-No WSL installed, cannot run docker on this machine.")
        node_status = statusQ.get()
        node_status[hpc] = 'Failed'
        statusQ.put(node_status)
        client.close()
        return
    elif 'Docker is not running' in lines[0]:
        start = True
    elif 'Docker is running' in lines[0]:
        start = False
    elif 'inactive' in lines[2]:
        start = True
    elif 'failed' in lines[2]:
        print("hpc"+str(hpc)+"-", end="")
        printformat(lines)
        printformat(errors)
        node_status = statusQ.get()
        node_status[hpc] = 'Failed'
        statusQ.put(node_status)
        client.close()
        return
    elif 'active' in lines[2]:
        start = False
    else:
        print("hpc"+str(hpc)+"-", end="")
        printformat(lines)
        printformat(errors)
        node_status = statusQ.get()
        node_status[hpc] = 'Failed'
        statusQ.put(node_status)
        client.close()
        return
    #Start Docker service if not started
    if start:
        print("hpc"+str(hpc)+"-Docker not running or inactive. Start to run Docker now.")
        #Command: Start docker
        stdin, stdout, stderr = client.exec_command("wsl sudo service docker start", timeout=60)
        lines = stdout.readlines()
        errors = stderr.readlines()
        if errors:
            printformat(errors)
            node_status = statusQ.get()
            node_status[hpc] = 'Failed'
            statusQ.put(node_status)
            client.close()
            return
        for i in range(0, 6, 1):
            time.sleep(10)
            stdin, stdout, stderr = client.exec_command("wsl service docker status")
            lines = stdout.readlines()
            errors = stderr.readlines()
            if errors:
                printformat(errors)
                node_status = statusQ.get()
                node_status[hpc] = 'Failed'
                statusQ.put(node_status)
                client.close()
                return
            if (len(lines)>2 and 'failed' in lines[2]):
                node_status = statusQ.get()
                node_status[hpc] = 'Failed'
                statusQ.put(node_status)
                client.close()
                return
            if 'Docker is running' in lines[0] or (len(lines)>2 and 'active' in lines[2]):
                node_status = statusQ.get()
                node_status[hpc] = 'Online'
                statusQ.put(node_status)
                break
    else:
        print("hpc"+str(hpc)+"-Docker already running.")  
    #Run Docker image if not running
    #Command: Check if image kosl/ihipp:v is in the list of containers
    stdin, stdout, stderr = client.exec_command("wsl bash -c 'docker ps | grep " + image + "'")
    lines = stdout.readlines()
    errors = stderr.readlines()
    if errors:
        if 'screen size is bogus. expect trouble' in errors[0]:
            pass
        else:
            printformat(errors)
            client.close()
            return
    if lines:
        print("hpc"+str(hpc)+"-Image " + image + " already running.")
    else:
        print("hpc"+str(hpc)+"-Now running " + image)
        #Command: Run Docker image.
        stdin, stdout, stderr = client.exec_command("wsl --exec bash -c '" + docker_run_command + "'")
        lines = stdout.readlines()
        errors = stderr.readlines()
        #if errors:
            #printformat(errors)
            #client.close()
            #return
    #Keep connection open until Docker image stops running due to inactivity
    while lines:
        time.sleep(5)
        #Command: Check if image kosl/ihipp:v is in the list of containers
        stdin, stdout, stderr = client.exec_command("wsl bash -c 'docker ps | grep " + image + "'")
        lines = stdout.readlines()
        errors = stderr.readlines()
        #print(host, stats(hpc), timestamp(), end='\r')
        if errors:
            printformat(errors)
            node_status = statusQ.get()
            node_status[hpc] = 'Failed'
            statusQ.put(node_status)
            client.close()
            return
    #exit
    stdin, stdout, stderr = client.exec_command("exit")
    lines = stdout.readlines()
    errors = stderr.readlines()
    if errors:
        printformat(errors)
    client.close()

def launcher():
    node_requirement = 2
    statusQ = Queue()
    global node_status
    statusQ.put(node_status)
    start_all_online = True
    status_line_prev = ''
    while True:
        #Check for active nodes
        nodes_online = 0
        node_status = statusQ.get()
        for hpc in range(61, 79):
            if node_status[hpc] != 'Failed' and ping(hpc):
                node_status[hpc] = 'Online'
                nodes_online+=1
            else:
                node_status[hpc] = 'Offline'
        if nodes_online < node_requirement:
            print("Not enough nodes online. Waking up.")
            for hpc in range(61,79):
                if node_status[hpc] == 'Offline':
                    send_magic_packet(hosts[int(hpc)],interface=interface)
                    for i in range(0, 6, 1):
                        time.sleep(10)
                        if ping(hpc):
                            node_status[hpc] = 'Online'
                            nodes_online+=1
                            break
                    if not ping(hpc):
                        node_status[hpc] = 'Failed'
                        print("hpc"+str(hpc)+"-Failed to wake up.")
                if nodes_online >= node_requirement:
                    break
        statusQ.put(node_status)
        procs = {}
        if start_all_online: #Run all online nodes. 
            nodes_to_run = nodes_online
        else: #Only run required number of online nodes. 
            nodes_to_run = node_requirement
        for hpc in node_status.keys():
            if node_status[hpc] == 'Online':
                print("Run hpc" + str(hpc))
                p = Process(target=run, args = (hpc, statusQ, ))
                procs[hpc] = p
                p.start()
                nodes_to_run -= 1
            if nodes_to_run == 0:
                break
        #Check if run(hpc) is terminated and restart subprocesses if number of nodes is less than node_requirement.
        while True:
            status_line_print = ''
            total_notebooks = 0
            for hpc in list(procs):
                procs[hpc].join(5)
                if procs[hpc].exitcode != None:
                    node_status = statusQ.get()
                    statusQ.put(node_status)
                    print("hpc"+str(hpc)+"-Stopped " + timestamp())
                    procs.pop(hpc)
                else:
                    status = stats(hpc)
                    #print(f"[{hpc},{status}]", end="", flush=True)
                    status_line_print += '[' + str(hpc) + ',' + str(status) + ']'
                    if status is not None:
                        total_notebooks += status
                    else:
                        #WSL not patched anymore.
                        print("hpc"+str(hpc)+"-Stopped " + timestamp())
                        procs[hpc].terminate()
                        procs.pop(hpc)
            #print(total_notebooks)
            status_line_print += str(total_notebooks)
            if status_line_print != status_line_prev:
                print(status_line_print)
                status_line_prev = status_line_print
            #We require minimum of 2 running nodes anytime. On average 3 notebooks per node
            if len(procs) < 2 or total_notebooks > 3*len(procs):
                node_requirement = max(math.ceil(total_notebooks/3), 2)
                print("Currently not enough nodes running. Terminating and starting again.")
                for hpc in procs:
                    procs[hpc].terminate()
                start_all_online = False
                break
            #print(node_status)

docker_run_command="""
docker run --detach \
-v /etc/hostname:/etc/hostname:ro \
-v /usr/lib/wsl:/usr/lib/wsl:ro \
-v /usr/local/cuda-11.4:/usr/local/cuda-11.4:ro \
-v /usr/local/lib:/usr/local/lib:ro \
-v /usr/local/include:/usr/local/include:ro \
-v /etc/OpenCL:/etc/OpenCL:ro \
-e DOCKER_STACKS_JUPYTER_CMD=notebook \
-p 8000:8888 """ + image

docker_pull_command = "docker pull "+image

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-a", "--all", dest="all", action="store_true",
                      help="select all nodes hpc[61:78]")
    
    parser.add_option("-q", "--quiet",
                      action="store_false", dest="verbose", default=True,
                      help="don't print status messages to stdout")

    parser.add_option("-p", "--ping", action="store_true",
                      help="Print ping status")

    parser.add_option("-r", "--run", action="store_true",
                      help="Run image "+image+" in nohup" )


    parser.add_option("-u", "--pull", action="store_true",
                      help="Pull image "+image )

    parser.add_option("-w", "--wake", action="store_true",
                      help="Wake on lan the node(s)")

    parser.add_option("-c", "--command", dest="command",
                      metavar="command", help="Execute ssh command")

    parser.add_option("-s", "--stats", action="store_true",
                      help="Return metrics of node (number of kernels currently running)")

    parser.add_option("-l", "--launch", action="store_true",
                      help="Start launcher (automatically turns on nodes and runs service based on metrics)")



    (options, args) = parser.parse_args()

    if options.all:
        if options.wake:
            send_magic_packet(*all_hosts,interface=interface)
        if options.ping:
            for hpc in range(61, 79):
                ping(hpc)
        if options.stats:
            for hpc in range(61, 79):
                print(stats(hpc))
        if options.launch:
            launcher()

    if args:
        for hpc in args:
            if options.wake:
                send_magic_packet(hosts[int(hpc)],interface=interface)
            if options.ping:
                ping(int(hpc))
            if options.command:
                ssh("hpc"+hpc, options.command)
            if options.pull:
                ssh("hpc"+hpc, docker_pull_command)
            if options.run:
                ssh("hpc"+hpc, docker_run_command)
            if options.stats:
                print(stats(int(hpc)))
            if options.launch:
                statusQ = Queue()
                statusQ.put(node_status)
                run(int(hpc), statusQ)
