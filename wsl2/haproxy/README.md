# Managment and load balancing of compute nodes

Farm of Docker notebooks running on Windows PCs under WSL2 
are load balanced using haproxy. 


Load balancing with HTTPS wrapping requires that server cookies
are having SameSite None and Secure cookie attributes in order
for Chrome 80 to work.


~~~sh
python3 -m venv local
local/bin/pip install wakeonlan paramiko
local/bin/python hpc-node.py --help
~~~

# Running:

Change passwords at xxxxx marks.
Runs and controls all awakened nodes, keeping a minimum of `node_requerement` nodes running.
~~~sh
local/bin/python hpc-node.py --launch --all
~~~
